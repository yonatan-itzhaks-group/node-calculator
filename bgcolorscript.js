const fs = require('fs');
const cp = require('child_process')

let color = (process.argv[2]);
fs.readFile('./' + color + '.css', function read(err){
    if (err){
        throw 'File "' + color + '" does not exist, try "red" or "green"';
    }
    fs.copyFile('./' + color + '.css', 'app.css', (err) => {
        if (err){
            throw 'Unable to copy files';
        }
    })
    cp.fork(__dirname + '/app.js');
})